from django.contrib import admin
from .models import Past_Project, Student, Date_Setting,Document

admin.site.register(Past_Project)
admin.site.register(Student)
admin.site.register(Date_Setting)
admin.site.register(Document)


