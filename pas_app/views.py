import gensim
from django.contrib import messages
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import auth,User
from django.core import serializers
from django.core.mail import send_mail
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.shortcuts import render, redirect,reverse,render_to_response
from django.http import HttpResponseRedirect
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage
from io import StringIO
import docx2txt
import numpy as np
from django.views.generic import View
from django.core.files.storage import FileSystemStorage
from datetime import datetime
import nltk
from nltk.tokenize import word_tokenize, sent_tokenize
from django.template import RequestContext
from pas_app.models import Student, Past_Project,Date_Setting,Document
from pas_app.render import Render


# Create your views here.


def home(request):
    return render(request,'index.html')



def panel_login_page(request):
    return render(request, 'login.html')



@csrf_exempt
def panel_login(request):
    if request.method == 'POST':
        username = request.POST["usname"]
        password = request.POST["pass"]
        request.session['username'] = username
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_staff==True:
                login(request, user)
                return render(request,'panel_home.html')
            else:
                messages.warning(request, 'Not a Staff !!')
                return HttpResponseRedirect(reverse('panel_login'))
        else:
            messages.warning(request,'User does not exist/ Password incorrect.Contact Admin')
            return HttpResponseRedirect(reverse('panel_login'))

    else:
        messages.warning(request, 'Something went wrong.')
        return HttpResponseRedirect(reverse('panel_login'))

def Logout(request):
    u=request.session.get('username')
    print(u)
    us=User.objects.get(username=u)
    if us.is_staff == True:
        logout(request)
        messages.success(request, 'Logged out successfully')
        return redirect('panel_login')
    else:
        logout(request)
        messages.success(request, 'Logged out successfully')
        return redirect('stu_login')










def stu_login_page(request):
    print('login page')
    return render(request, 'stu_login.html')

@csrf_exempt
def upload_page(request):
    if request.method == 'POST':
        print('upload page')
        date = Date_Setting.objects.get(section="Login")
        start = date.start_date.strftime("%Y-%m-%d %H:%M:%S")
        end = date.end_date.strftime("%Y-%m-%d %H:%M:%S")
        now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        #print("start : ",start)
        #print("End : ",end)
        #print("now : ",now)
        if (start <= now) and (end >= now):
            username = request.POST["username"]
            password = request.POST["password"]
            request.session['username'] = username
            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_staff == False:
                    stu=Student.objects.get(username=username)
                    course=Document.objects.get(description="course_details")
                    format=Document.objects.get(description="proposal_format")
                    login(request, user)
                    return render(request,'upload.html', {"student":stu,"course":course,"format":format})
                else:
                    messages.warning(request, 'Student Login only')
                    return render(request,'stu_login.html')
            else:
                messages.warning(request,'User does not exist/ Password incorrect.Contact Admin')
                return render(request,'stu_login.html')
        elif now < start:
            messages.warning(request, 'Sorry, Site Accessing time not started yet')
            return redirect('stu_login')
        else:
            messages.warning(request, 'Sorry, Site Accessing time closed')
            return redirect('stu_login')

    else:
        messages.warning(request, 'Something Went Wrong !')
        return render(request,'stu_login.html')
@login_required
def upload(request):
    if request.method == 'POST':
        if request.FILES['document']:
            username = request.session.get('username')
            topi=request.POST["topic"]
            topic=topi.upper()
            s = Student.objects.get(username=username)
            status=s.status
            print(status)
            if status== "Nothing Uploaded" or status=="Rejected":
                print("Entered")
                abstract=request.FILES['document']
                flag=0
                checked = 'check' in request.POST
                if checked == True:
                    try:
                        if request.FILES['basepaper']:
                            flag=1
                            basepaper=request.FILES['basepaper']
                            #print(basepaper)
                    except:
                        flag=0
                        #print("No basepaper")
                MY_TEXT1 = docx2txt.process(abstract)
                with open("media/text_files/output2.txt", "w", encoding="utf-8") as file:
                    file.write(MY_TEXT1)
                    file.close()

                previous=Past_Project.objects.all()
                similar=False
                for item in previous:
                    report=item.document
                    print(item.name)
                    #with open(report.path, "rb") as filehandle:
                    rsrcmgr = PDFResourceManager()
                    retstr = StringIO()
                    codec = 'utf-8'
                    laparams = LAParams()
                    device = TextConverter(rsrcmgr, retstr, codec=codec, laparams=laparams)
                    fp = open(report.path, 'rb')
                    interpreter = PDFPageInterpreter(rsrcmgr, device)
                    password = ""
                    maxpages = 0
                    caching = True
                    pagenos = set()
                    for pagenumber, page in enumerate(
                            PDFPage.get_pages(fp, pagenos, maxpages=maxpages, password=password, caching=caching,
                                              check_extractable=True)):
                        if pagenumber >= 0 and pagenumber <= 3:
                            continue
                        elif pagenumber == 4:
                            interpreter.process_page(page)
                        else:
                            break
                    text = retstr.getvalue()
                    fp.close()
                    device.close()
                    retstr.close()
                    #print(text)
                    with open("media/text_files/output.txt", "w", encoding="utf-8") as file:
                        file.write(text)
                        file.close()
                    file_docs = []
                    file2_docs = []
                    avg_sims = []
                    with open('media/text_files/output2.txt',"r",encoding='utf-8') as f:
                        tokens = sent_tokenize(f.read())
                        for line in tokens:
                            file_docs.append(line)
                    #print("Inputting file:\n", file_docs)
                    length_doc1 = len(file_docs)
                    #print("length of inputting file  :", length_doc1)

                    gen_docs = [[w.lower() for w in word_tokenize(text)]
                                for text in file_docs]
                    #print("to lower", gen_docs)
                    dictionary = gensim.corpora.Dictionary(gen_docs)
                    #print("input file to dictionary :\n", dictionary)
                    #print(dictionary[id] for id, freq in dictionary)
                    corpus = [dictionary.doc2bow(gen_doc) for gen_doc in gen_docs]
                    #print("doc2bow : \n", corpus)
                    tf_idf = gensim.models.TfidfModel(corpus)
                    #print("td idf :", tf_idf)
                    sims = gensim.similarities.Similarity('workdir/', tf_idf[corpus],
                                                          num_features=len(dictionary))
                    #print("sims  :", sims)
                    # 2nd docx to txt
                    with open('media/text_files/output.txt',encoding='utf-8') as f:
                        tokens = sent_tokenize(f.read())
                        for line in tokens:
                            file2_docs.append(line)

                    for line in file2_docs:
                        query_doc = [w.lower() for w in word_tokenize(line)]
                        query_doc_bow = dictionary.doc2bow(query_doc)
                        query_doc_tf_idf = tf_idf[query_doc_bow]
                        #print("2nd file\n",query_doc_tf_idf)
                        #print('Comparing Result:', sims[query_doc_tf_idf])
                        sum_of_sims = (np.sum(sims[query_doc_tf_idf], dtype=np.float32))
                        #print("sum of sims : ",sum_of_sims)
                        #print("len of first file doc",len(file_docs))
                        avg = sum_of_sims / len(file_docs)
                        #print(f'avg: {sum_of_sims / len(file_docs)}')
                        avg_sims.append(avg)
                    total_avg = np.sum(avg_sims, dtype=np.float)
                    print(total_avg)
                    percentage_of_similarity = round(float(total_avg) * 100)
                    if percentage_of_similarity >= 100:
                        percentage_of_similarity = 100
                    print("percentage :", percentage_of_similarity)
                    if total_avg >1.0:
                        name=item.name
                        regno=item.regno
                        batch=item.batch
                        similar=True
                        break

                if similar==True:
                    print("similar with")
                    print(name)
                    return render_to_response('similar_msg.html',{"name":name,"regno":regno,"batch":batch})
                else:
                    date = datetime.now()
                    upload_date = date.strftime("%Y-%m-%d %H:%M:%S")
                    if status=="Nothing Uploaded":
                        if flag==1:
                            s.topic=topic
                            s.first_upload_date=upload_date
                            s.last_upload_date=upload_date
                            s.abstract=abstract
                            s.base_paper=basepaper
                            s.status="Uploaded"
                            s.save()
                        else:
                            s.topic=topic
                            s.first_upload_date=upload_date
                            s.last_upload_date=upload_date
                            s.abstract=abstract
                            s.status="Uploaded"
                            s.save()
                    else:
                        reject_count=s.reject_no +1
                        if flag==1:
                            s.topic = topic
                            s.last_upload_date = upload_date
                            s.abstract = abstract
                            s.base_paper = basepaper
                            s.reject_no=reject_count
                            s.status = "Uploaded"
                            s.save()
                        else:
                            s.topic = topic
                            s.last_upload_date = upload_date
                            s.abstract = abstract
                            s.reject_no=reject_count
                            s.status = "Uploaded"
                            s.base_paper=None
                            s.save()
                #print(abstract)
                #print(checked)
                return render(request,'upload.html')
            else:
                print("Already uploaded")
                return render(request,'already_msg.html')
    else:
        messages.warning(request, 'Something went wrong.')
        return render(request, 'stu_login.html')


def upload_check(request):
    if request.method == 'POST' and request.FILES['document']:

        regno = request.POST["c_regno"]
        date = datetime.now()
        upload_date = date.strftime("%Y-%m-%d %H:%M:%S")
        topic = request.POST["topic"]
        abstract = request.FILES["document"]
        s = Student.objects.filter(regno=regno)
        for item in s:
            item.topic = topic
            item.abstract = abstract
            item.upload_date=upload_date
            item.save()
        messages.success(request,'Uploaded Successfull')
        return render(request, 'upload.html')
    else:
        messages.warning(request,"something went wrong")
        return render(request, 'upload.html')


@csrf_exempt
def profile(request,regno):
    stu=Student.objects.get(regno=regno)
    abstract = stu.abstract
    # print(abstract)
    MY_TEXT1 = docx2txt.process(abstract)
    # print(MY_TEXT1)
    with open("media/text_files/output1.txt", "w", encoding="utf-8") as file:
        file.write(MY_TEXT1)
        file.close()
    #print(stu)
    #print(stu.base_paper)
    return render_to_response("student_detailed.html",{'student':stu})




#just render the registration template
def stu_reg(request):
    return render(request, 'stu_reg.html')


#student registration
def registration(request):
    if request.method=='POST':
        date = Date_Setting.objects.get(section="Registration")
        start = date.start_date.strftime("%Y-%m-%d %H:%M:%S")
        end=date.end_date.strftime("%Y-%m-%d %H:%M:%S")
        now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        #print("start : ",start)
        #print("End : ",end)
        #print("now : ",now)
        if (start <= now) and (end >= now) :
            first_nam=request.POST["r_firstname"]
            last_nam=request.POST["r_lastname"]
            first_name=first_nam.upper()
            last_name=last_nam.upper()
            name=first_name+' '+last_name
            regn = request.POST["r_regno"]
            regno = regn.upper()
            rolln=request.POST["r_rollno"]
            rollno=rolln.upper()
            admno=request.POST["r_admno"]
            email = request.POST["r_email"]
            username=request.POST["r_username"]
            batch=request.POST["r_batch"]
            password=request.POST["r_password"]
            mo=request.POST["r_telphone"]
            mob=str(mo).strip()
            confirm_password = request.POST["r_confirmpassword"]
            date=datetime.now()
            last_upload_date=first_upload_date = date.strftime("%Y-%m-%d %H:%M:%S")
            if Student.objects.filter(email=email).exists() and User.objects.filter(email=email).exists():
                messages.warning(request,'Mail id already exists!')
                return render(request, 'stu_reg.html')
            else:
                if Student.objects.filter(regno=regno).exists():
                    messages.warning(request,'Already Register? Check your RegNo')
                    return render(request, 'stu_reg.html')
                else:
                    if Student.objects.filter(rollno=rollno).exists():
                        messages.warning(request, 'Already Register? Check your RollNo')
                        return render(request, 'stu_reg.html')
                    else:
                        if Student.objects.filter(admission_no=admno).exists():
                            messages.warning(request, 'Already Register? Check your AdmissionNo')
                            return render(request, 'stu_reg.html')
                        else:
                            if Student.objects.filter(username=username).exists() and User.objects.filter(username=username).exists():
                                messages.warning(request, 'Username taken, Please type another one')
                                return render(request, 'stu_reg.html')
                            else:
                                if password != confirm_password:
                                    messages.warning(request, 'Password mismatch !!')
                                    return render(request, 'stu_reg.html')
                                else:
                                    if len(password) < 8:
                                        messages.warning(request, 'Password must contain atleast 8 characters')
                                        return render(request, 'stu_reg.html')
                                    else:
                                        fi_isalpha = password[0].isalpha()
                                        if all(c.isalpha() == fi_isalpha for c in password):
                                            messages.warning(request,
                                                             'Password must contain atleast a letter,a digit or a punctuation')
                                            return render(request, 'stu_reg.html')
                                        else:
                                            user = User.objects.create_user(username=username, password=password, email=email,
                                                                            first_name=first_name, last_name=last_name)
                                            user.save();
                                            stud=Student.objects.create(name=name,regno=regno,email=email,username=username,batch=batch,first_upload_date=first_upload_date,last_upload_date=last_upload_date,admission_no=admno,mob=mob,rollno=rollno)
                                            stud.save();
                                            messages.success(request,'Registration successfully done.')
                                            return redirect(reverse('stu_login'))
        elif now < start :
            messages.warning(request, 'Sorry, Registration time not started')
            return redirect('stu_login')
        else:
            messages.warning(request, 'Sorry, Registration time closed')
            return redirect('stu_login')

    else:
        messages.warning(request, 'Something went wrong')
        return render(request, 'stu_reg.html')

#student message to admin
def send_message(request):
    if request.method == 'POST':
        try:
            print('msg page')
            superuser = User.objects.get(is_superuser=True)
            superuser_email=superuser.email
            print(superuser_email)
            subject=request.POST["subject"]
            msgg=request.POST["message"]
            user=request.session.get('username')
            stud=Student.objects.get(username=user)
            stud_name=stud.name
            stud_mail=stud.email
            batch=stud.batch
            msg=" Hi Sir/Ma'am ,This is "+stud_name+"( "+str(batch)+" batch )<"+stud_mail+">\n              "+msgg
            print(stud_name)
            send_mail(subject, msg, stud_mail, [superuser_email], fail_silently=False)
            return render_to_response('msg_success.html',
                                      {'user': user})
        except:
            return render_to_response('msg_failure.html',
                                      {'user': user})
    else:
        messages.warning(request, 'Something went wrong.')
        return HttpResponseRedirect(reverse('stu_login'))


def send_to_student(request):
    if request.method=="POST":
        try:
            ad=User.objects.get(is_superuser=True)
            superuser_mail=ad.email
            regn=request.POST["regno"]
            regno=regn.upper()
            subject = request.POST["subject"]
            msgg=request.POST["message"]
            stu=Student.objects.get(regno=regno)
            stu_name=stu.name
            stu_mail=stu.email
            print(stu_mail)
            print(superuser_mail)
            msg=" Hi "+stu_name+" , This is from MCA Dept.   "+msgg
            send_mail(subject, msg, superuser_mail , [stu_mail], fail_silently=False)
            return render(request,"msg_success.html")
        except:
            print("net connection problem")
            return render(request,"stu_msg_failure.html")

    else:
        messages.warning(request, 'Something went wrong.')
        return HttpResponseRedirect(reverse('panel_login'))


@csrf_protect
def approval_requests(request):
    stu=Student.objects.filter(status="Uploaded").order_by('last_upload_date')
    print(stu)
    return  render_to_response('request_page.html',{'students': stu})



def approved_list(request):
    return render(request,"approve_list_page.html")


def approved_list_show(request):
    if request.method=="POST":
        batch=request.POST['batch']
        stu=Student.objects.filter(batch=batch,status="Approved")
        if not stu:
            #print(stu)
            return render(request,"nothing.html")
        else:
            request.session['batch'] = batch
            request.session['type']="Approved"
            return render_to_response('approved_list_show.html',{"student": stu,"batch":batch})
    else:
        print("Not a post method")
        messages.warning(request, 'Something went wrong.')
        return render(request, 'panel_login.html')


class Pdf(View):
    def get(self, request):
        batch = request.session.get('batch')
        type=request.session.get('type')
        if type=="Approved":
            subject="Approved List (KTU - "+batch+" Admissions)"
            stu = Student.objects.filter(batch=batch,status="Approved")
        else:
            subject="Rejected List (KTU - "+batch+" Admissions)"
            stu = Student.objects.filter(batch=batch,status="Rejected")
        print(stu)
        params = {'student': stu,"subject":subject}

        return Render.render('pdf1.html', params)


def rejected_list(request):
    return render(request, "reject_list_page.html")


def rejected_list_show(request):
    if request.method=="POST":
        batch=request.POST['batch']
        stu=Student.objects.filter(batch=batch,status="Rejected")
        if not stu:
            #print(stu)
            return render(request,"nothing.html")
        else:
            request.session['batch'] = batch
            request.session['type']="Rejected"
            return render_to_response('rejected_list_show.html',{"student": stu,"batch":batch})
    else:
        print("Not a post method")
        messages.warning(request, 'Something went wrong.')
        return render(request, 'panel_login.html')



def student_status(request):
    return render(request, "search_student.html")


def student_details_show(request):
    if request.method=="POST":
        reg=request.POST['regno']
        regno=reg.upper()
        try:
            stu=Student.objects.get(regno=regno)
            if not stu:
                #print(stu)
                return render(request,"nothing.html")
            else:
                abstract = stu.abstract
                # print(abstract)
                MY_TEXT1 = docx2txt.process(abstract)
                # print(MY_TEXT1)
                with open("media/text_files/output1.txt", "w", encoding="utf-8") as file:
                    file.write(MY_TEXT1)
                    file.close()
                return render_to_response('student_detail.html',{"student": stu})
        except:
            return render(request, "nothing.html")
    else:
        print("Not a post method")
        messages.warning(request, 'Something went wrong.')
        return render(request, 'panel_login.html')


#
@csrf_protect
def waiting_list(request):
    stu=Student.objects.filter(status="Waiting").order_by('last_upload_date')
    print(stu)
    if not stu:
        return render(request,"nothing.html")
    else:
        return  render_to_response('waiting_list.html',{'students': stu})


@csrf_exempt
def waiting_list_detailed(request,regno):
    print(regno)
    s=Student.objects.get(regno=regno)
    abstract = s.abstract
    #print(abstract)
    MY_TEXT1 = docx2txt.process(abstract)
    #print(MY_TEXT1)
    with open("media/text_files/output1.txt", "w", encoding="utf-8") as file:
        file.write(MY_TEXT1)
        file.close()
    print(s)
    print(s.name)
    print(s.regno)
    request.session['selected_student'] = regno
    print("success ")
    return render_to_response("waiting_list_detailed.html",{'student':s})


class Pdf1(View):
    def get(self, request):
        user = request.session.get('username')
        s=Student.objects.get(username=user)
        if s.status=="Approved":
            print(s.reject_no)
            proposal=s.reject_no
            p=proposal+1
            batch=s.batch
            first=str(batch+2)
            second=str(batch+3)
            acadamic=first+"-"+second
            date=datetime.today().strftime('%d-%m-%Y')
            print(s)
            params = {'student': s,'no':p,'acadamic':acadamic,'date':date}
            return Render.render('pdf2.html',params)
        else:
            return render(request,"not_approved.html")

@csrf_exempt
def approve(request,regno):
    if request.method=="POST":
        print("Approve confirm page")
        return render_to_response('approved_page.html',{'regno':regno})
    else:
        print("not post method in approve function")

@csrf_exempt
def wait(request,regno):
    if request.method == "POST":
        print("Waiting confirm page")
        return render_to_response('waiting_page.html',{'regno':regno})
    else:
        print("not post method in wait function")

@csrf_exempt
def reject(request,regno):
    if request.method == "POST":
        print("to reject confirm page")
        return render_to_response('reject_page.html',{'regno':regno})
    else:
        print("not post method in reject function")

@csrf_exempt
def approve_confirm(request,regno):
    if request.method == "POST":
        notes=request.POST['notes']
        print(regno)
        print(notes)
        stu = Student.objects.get(regno=regno)
        stu_name = stu.name
        stu_mail = stu.email
        subject="Abstract Approved"
        print(stu_mail)
        ad = User.objects.get(is_superuser=True)
        superuser_mail = ad.email
        stu.status="Approved"
        stu.notes=notes
        stu.save()
        msg = " Hi " + stu_name + " , This is from MCA Dept. Your Abstract ("+stu.topic+") has been approved,please download document available in your login and submit it to project coordinator\n\nNote : " + notes+"\n\nThankYou."
        try:
            send_mail(subject, msg, superuser_mail, [stu_mail], fail_silently=False)
            return render(request, 'confirm.html')
        except:
            print("net connection problem")
            return render(request, "stu_msg_failure.html")
    else:
        print("Not post method in approve_confirm view")

@csrf_exempt
def wait_confirm(request,regno):
    if request.method == "POST":
        notes=request.POST['wait_reason']
        print(regno)
        print(notes)
        stu = Student.objects.get(regno=regno)
        stu_name = stu.name
        stu_mail = stu.email
        subject="Moved to Waiting List"
        print(stu_mail)
        ad = User.objects.get(is_superuser=True)
        superuser_mail = ad.email
        stu.status="Waiting"
        stu.notes = notes
        stu.save()
        msg = " Hi " + stu_name + " , This is from MCA Dept. Your Abstract ("+stu.topic+") has been added to waiting list.\n\nReason : " + notes +'\nThe status(Approve/Reject) will be inform later\n\nThankYou'
        try:
            send_mail(subject, msg, superuser_mail, [stu_mail], fail_silently=False)
            return render(request, 'confirm.html')
        except:
            print("net connection problem")
            return render(request, "stu_msg_failure.html")
    else:
        print("Not post method in wait_confirm view")


@csrf_exempt
def reject_confirm(request,regno):
    if request.method == "POST":
        notes=request.POST['reject_reason']
        print(regno)
        print(notes)
        stu = Student.objects.get(regno=regno)
        stu_name = stu.name
        stu_mail = stu.email
        subject="Rejected"
        print(stu_mail)
        ad = User.objects.get(is_superuser=True)
        superuser_mail = ad.email
        stu.status="Rejected"
        stu.notes = notes
        stu.save()
        msg = " Hi " + stu_name + " , This is from MCA Dept. Your Abstract ("+stu.topic+") has been rejected.\n\nReason : " + notes +'\nPlease upload a new one\n\nThankYou'
        try:
            send_mail(subject, msg, superuser_mail, [stu_mail], fail_silently=False)
            return render(request, 'confirm.html')
        except:
            print("net connection problem")
            return render(request, "stu_msg_failure.html")
    else:
        print("Not post method in wait_confirm view")

@csrf_exempt
def wait_approve(request,regno):
    if request.method=="POST":
        print(" to wait Approve confirm page")
        return render_to_response('wait_approved_page.html',{'regno':regno})
    else:
        print("not post method in approve function")

@csrf_exempt
def wait_reject(request,regno):
    if request.method=="POST":
        print(" to wait Approve confirm page")
        return render_to_response('wait_reject_page.html',{'regno':regno})
    else:
        print("not post method in approve function")


@csrf_exempt
def wait_approve_confirm(request,regno):
    if request.method == "POST":
        notes=request.POST['notes']
        print(regno)
        print(notes)
        stu = Student.objects.get(regno=regno)
        stu_name = stu.name
        stu_mail = stu.email
        subject="Abstract Approved"
        print(stu_mail)
        ad = User.objects.get(is_superuser=True)
        superuser_mail = ad.email
        stu.status="Approved"
        stu.notes = notes
        stu.save()
        msg = " Hi " + stu_name + " , This is from MCA Dept. Your Abstract has been approved ("+stu.topic+"),please download document available in your login and submit it to project coordinator\n\nNote : " + notes+"\n\nThankyou"
        try:
            send_mail(subject, msg, superuser_mail, [stu_mail], fail_silently=False)
            return render(request, 'confirm.html')
        except:
            print("net connection problem")
            return render(request, "stu_msg_failure.html")
    else:
        print("Not post method in wait_approve_confirm view")



@csrf_exempt
def wait_reject_confirm(request,regno):
    if request.method == "POST":
        notes=request.POST['reject_reason']
        print(regno)
        print(notes)
        stu = Student.objects.get(regno=regno)
        stu_name = stu.name
        stu_mail = stu.email
        subject="Rejected"
        print(stu_mail)
        ad = User.objects.get(is_superuser=True)
        superuser_mail = ad.email
        stu.status="Rejected"
        stu.notes = notes
        stu.save()
        msg = " Hi " + stu_name + " , This is from MCA Dept. Your Abstract has been rejected ("+stu.topic+").\n\nReason : " + notes +'\nPlease upload a new one\n\nThankYou'
        try:
            send_mail(subject, msg, superuser_mail, [stu_mail], fail_silently=False)
            return render(request, 'confirm.html')
        except:
            print("net connection problem")
            return render(request, "stu_msg_failure.html")
    else:
        print("Not post method in wait_confirm view")



