from django.urls import path
from django.contrib.auth import views as auth_views
from pas_app.views import upload, stu_login_page, stu_reg, registration, upload_page, profile, panel_login, \
    send_message, Logout, send_to_student, approval_requests, approved_list, approved_list_show, Pdf, rejected_list, \
    rejected_list_show, student_status, student_details_show, waiting_list, waiting_list_detailed, Pdf1, approve, \
    approve_confirm, wait, wait_confirm, reject, reject_confirm, wait_approve, wait_reject, wait_approve_confirm, \
    wait_reject_confirm
from . import views
from django.contrib.auth.views import login
from django.conf import settings
from django.conf.urls.static import static
urlpatterns = [
    path('',views.home,name='home'),
    path('panel/',views.panel_login_page,name='panel_login'),
    path('panel/login',panel_login,name='panel_home'),
    path('panel/login/request',approval_requests,name='approval_requests'),
    path('panel/login/request/<str:regno>',profile,name="profile"),
    path('panel/login/request/<str:regno>/approve',approve,name='approve'),
    path('panel/login/request/<str:regno>/approve/approve_confirm',approve_confirm,name='approve_confirm'),
    path('panel/login/request/<str:regno>/wait',wait,name='wait'),
    path('panel/login/request/<str:regno>/wait/wait_confirm',wait_confirm,name='wait_confirm'),
    path('panel/login/request/<str:regno>/reject',reject,name='reject'),
    path('panel/login/request/<str:regno>/reject/reject_confirm',reject_confirm,name='reject_confirm'),
    path('panel/login/approved_list',approved_list,name='approved_list'),
    path('panel/login/approved_list_show',approved_list_show,name="approved_list_show"),
    path('panel/login/approved_list_show/download',Pdf.as_view(),name="approved_list_download"),
    path('panel/login/rejected_list',rejected_list,name='rejected_list'),
    path('panel/login/rejected_list_show',rejected_list_show,name="rejected_list_show"),
    path('panel/login/rejected_list_show/download',Pdf.as_view(),name="rejected_list_download"),
    path('panel/login/waiting_list',waiting_list,name='waiting_list'),
    path('panel/login/waiting_list/<str:regno>',waiting_list_detailed,name='waiting_list_detailed'),
    path('panel/login/waiting_list/<str:regno>/approve',wait_approve,name='wait_approve'),
    path('panel/login/waiting_list/<str:regno>/approve/confirm',wait_approve_confirm,name='wait_approve_confirm'),
    path('panel/login/waiting_list/<str:regno>/reject',wait_reject,name='wait_reject'),
    path('panel/login/waiting_list/<str:regno>/reject/confirm',wait_reject_confirm,name='wait_reject_confirm'),
    path('panel/login/stud_details',student_status,name="stud_details"),
    path('panel/login/student_details_show',student_details_show,name="student_details_show"),
    path('panel/send_to_student',send_to_student,name='send_to_student'),
    path('panel/valuate',profile,name='valuate'),
    path('stu_login/',stu_login_page,name='stu_login'),
    path('logout/', Logout, name='userlogout'),
    path('stu_reg/', stu_reg, name='stu_reg'),
    path('stu_reg/registration', registration, name='registration'),
    path('stu_login/upload_page',upload_page,name='upload_page'),
    path('stu_login/Pdf_document',Pdf1.as_view(),name='Pdf_document'),
    path('stu_login/upload',upload,name='upload'),
    path('stu_login/send_message',send_message,name='send_message'),

    path('password-reset/', auth_views.PasswordResetView.as_view(template_name="registration/password_reset_form.html",
                                                                 email_template_name="registration/password_reset_email.html",
                                                                 success_url='/password-reset-done/'),
         name='password_reset'),

    path('password-reset-done/',
         auth_views.PasswordResetDoneView.as_view(template_name="registration/password_reset_done.html"),
         name='password_reset_done'),

    path('password-reset-confirm/<uidb64>/<token>/',
         auth_views.PasswordResetConfirmView.as_view(template_name="registration/password_reset_confirm.html",
                                                     success_url='/password-reset-complete/'),
         name="password_reset_confirm"),

    path('password-reset-complete/',
         auth_views.PasswordResetCompleteView.as_view(template_name='registration/password_reset_complete.html'),
         name='password_reset_complete'),
    ]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)