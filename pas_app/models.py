from django.db import models
from django.core.validators import RegexValidator

# Create your models here.



class Past_Project(models.Model):
    name = models.CharField(max_length=30, default="")
    regno=models.CharField(max_length=12,unique=True)
    batch=models.IntegerField(default=2016)
    domain=models.CharField(max_length=30, default="")
    document=models.FileField(upload_to='files/')
    def __str__(self):
        return self.regno

class Student(models.Model):
    name=models.CharField(max_length=30,default="")
    regno=models.CharField(max_length=12,unique=True)
    email = models.EmailField(unique=True)
    username = models.CharField(max_length=30, unique=True)
    batch=models.IntegerField()
    first_upload_date=models.DateTimeField()
    last_upload_date=models.DateTimeField()
    topic=models.CharField(max_length=100,default="")
    domain = models.CharField(max_length=30, default="")
    abstract=models.FileField(upload_to='abstract/')
    base_paper = models.FileField(upload_to='base_paper/')
    status=models.CharField(max_length=20,default="Nothing Uploaded")
    reject_no=models.IntegerField(default=0)
    guide=models.CharField(max_length=30,default="Not Assigned")
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                                 message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    mob = models.CharField(validators=[phone_regex], max_length=17, blank=True)
    admission_no=models.CharField(max_length=6,unique=True)
    rollno=models.CharField(max_length=12,unique=True)
    notes=models.CharField(max_length=300,default="")
    def __str__(self):
        return self.regno


class Date_Setting(models.Model):
    section = models.CharField(max_length=30, unique=True)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    def __str__(self):
        return self.section

class Document(models.Model):
    description = models.CharField(max_length=30, unique=True)
    document = models.FileField(upload_to='formats/')
    def __str__(self):
        return self.description
