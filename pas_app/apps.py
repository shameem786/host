from django.apps import AppConfig


class PasAppConfig(AppConfig):
    name = 'pas_app'
