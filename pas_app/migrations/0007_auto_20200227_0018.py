# Generated by Django 2.0 on 2020-02-26 18:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pas_app', '0006_auto_20200227_0016'),
    ]

    operations = [
        migrations.AlterField(
            model_name='student',
            name='dob',
            field=models.DateField(default='1996-12-10'),
        ),
    ]
