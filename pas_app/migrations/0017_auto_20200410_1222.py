# Generated by Django 2.0 on 2020-04-10 12:22

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pas_app', '0016_documents'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Documents',
            new_name='Document',
        ),
    ]
